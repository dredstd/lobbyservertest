﻿namespace WebApp.Models;

public struct LobbyDto
{
	public string LobbyId { get; set; }
	public UserDto[] Users { get; set; }
}