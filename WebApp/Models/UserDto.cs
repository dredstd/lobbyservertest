﻿namespace WebApp.Models;

public struct UserDto
{
	public string UserId { get; set; }

	public override string ToString()
	{
		return $"UserDTO {UserId}";
	}
}