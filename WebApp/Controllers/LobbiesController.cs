﻿using System.Net.WebSockets;
using AutoMapper;
using Core;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using WebApp.Service.Utils;

namespace WebApp.Controllers;

[Authorize]
[ApiController]
[Route("[controller]/[action]")]
public class LobbiesController : Controller
{
	private readonly LobbiesThreadSafe _lobbies;
	private readonly IMapper _mapper;
	private readonly GameConfig _gameConfig;
	private readonly ILoggerFactory _loggerFactory;

	public LobbiesController(LobbiesThreadSafe lobbies, IMapper mapper, GameConfig gameConfig,
		ILoggerFactory loggerFactory)
	{
		_lobbies = lobbies;
		_mapper = mapper;
		_gameConfig = gameConfig;
		_loggerFactory = loggerFactory;
	}

	[HttpGet]
	public IActionResult GetAll()
	{
		LobbyDto[] lobbyDto = _mapper.Map<LobbyDto[]>(_lobbies.GetLobbies().Select(lobby => lobby.Lobby));
		return new JsonResult(lobbyDto);
	}

	[HttpPost]
	public async Task<IActionResult> CreatePVPLobby()
	{
		User user = await HttpContext.GetUser();
		Lobby lobby = new Lobby(user, 2);
		PVPGameLobby gameLobby = new PVPGameLobby(lobby, new PVPGame(_gameConfig),
			new WebsocketGroup(_loggerFactory.CreateLogger<WebsocketGroup>()));
		_lobbies.AddLobby(gameLobby);
		LobbyDto lobbyDto = _mapper.Map<LobbyDto>(lobby);
		return new JsonResult(lobbyDto);
	}

	[HttpGet]
	public async Task LobbyWebsocketEvents(string lobbyId)
	{
		if (HttpContext.WebSockets.IsWebSocketRequest)
		{
			IGameLobby? gameLobby = _lobbies.GetLobby(lobbyId);
			if (gameLobby == null)
			{
				HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
				return;
			}

			User user = await HttpContext.GetUser();
			if (!gameLobby.Lobby.Users.Any(lobbyUser => lobbyUser.Equals(user)))
			{
				HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
				return;
			}

			using WebSocket webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();
			gameLobby.WebsocketGroup.AddClient(webSocket, user);
			while (webSocket.State is not WebSocketState.Closed or WebSocketState.Aborted)
			{
				await Task.Delay(100, HttpContext.RequestAborted);
			}
		}
		else
		{
			HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
		}
	}

	[HttpPut]
	public async Task<IActionResult> LeaveLobby(string lobbyId)
	{
		IGameLobby? gameLobby = _lobbies.GetLobby(lobbyId);
		if (gameLobby == null)
		{
			return new NotFoundResult();
		}

		User user = await HttpContext.GetUser();
		if (!gameLobby.Lobby.Users.Any(lobbyUser => lobbyUser.Equals(user)))
		{
			return new NotFoundResult();
		}

		gameLobby.Lobby.Leave(user);
		if (gameLobby.Lobby.Users.Count == 0)
		{
			_lobbies.RemoveLobby(gameLobby);
		}

		return Ok();
	}

	[HttpPut]
	public async Task<IActionResult> JoinLobby(string lobbyId)
	{
		IGameLobby? gameLobby = _lobbies.GetLobby(lobbyId);
		if (gameLobby == null)
		{
			return new NotFoundResult();
		}

		User user = await HttpContext.GetUser();
		if (gameLobby.Lobby.Users.Any(lobbyUser => lobbyUser.Equals(user)))
		{
			return Problem("Already joined");
		}

		gameLobby.Lobby.Join(user);
		LobbyDto lobbyDto = _mapper.Map<LobbyDto>(gameLobby.Lobby);
		return new JsonResult(lobbyDto);
	}

	[HttpDelete]
	public async Task<IActionResult> CloseLobby(string lobbyId)
	{
		User user = await HttpContext.GetUser();
		IGameLobby? gameLobby = _lobbies.GetLobby(lobbyId);
		if (gameLobby == null)
		{
			return new NotFoundResult();
		}

		if (gameLobby.Lobby.Host.Equals(user))
		{
			_lobbies.RemoveLobby(gameLobby);
		}

		return Ok();
	}
}