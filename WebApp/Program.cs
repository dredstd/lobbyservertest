using WebApp.Config;
using WebApp.Service;



WebApplicationBuilder webApplicationBuilder = WebApplication.CreateBuilder(args);

Startup startup = new(new ServerConfig());
startup.ConfigureServices(webApplicationBuilder.Services);

var app = webApplicationBuilder.Build();
startup.Configure(app, app.Environment);

app.Run();