namespace WebApp.Config;

public class ServerConfig
{
	public string OpenIdAuthority { get; init; } = "https://localhost:5000";
	public string OpenIdClientId { get; init; } = "interactive";
	public string OpenIdClientSecret { get; init; } = "49C1A7E1-0C79-4A89-A3D6-A37998FB86B0";
}