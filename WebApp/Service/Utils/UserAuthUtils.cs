﻿using System.Security.Authentication;
using System.Security.Claims;
using Core;
using Microsoft.AspNetCore.Authentication;

namespace WebApp.Service.Utils;

public static class UserAuthUtils
{
	public static async Task<User> GetUser(this HttpContext httpContext)
	{
		AuthenticateResult auth = await httpContext.AuthenticateAsync();
		ClaimsIdentity? principalIdentity = (ClaimsIdentity?)auth.Principal?.Identity;
		string? userId = principalIdentity?.Claims.FirstOrDefault(claim => claim.Type == "sub")?.Value;
		if (userId == null)
		{
			throw new AuthenticationException("have no sub claim");
		}

		return new User(userId);
	}
}