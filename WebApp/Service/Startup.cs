﻿using AutoMapper;
using Core;
using Microsoft.AspNetCore.StaticFiles;
using WebApp.Config;
using WebApp.Models;

namespace WebApp.Service;

public class Startup
{
	private readonly ServerConfig _serverConfig;
	const string AllowEveryOrigins = "_AllowEveryOrigins";

	public Startup(ServerConfig serverConfig)
	{
		_serverConfig = serverConfig;
	}

	public void ConfigureServices(IServiceCollection services)
	{
		services.AddCors(options =>
		{
			options.AddPolicy(name: AllowEveryOrigins,
				policy =>
				{
					policy.WithOrigins("https://game.tonlanders.com");
					policy.AllowAnyHeader();
					policy.AllowAnyMethod();
					policy.SetIsOriginAllowedToAllowWildcardSubdomains();
					policy.AllowCredentials();
				});
		});
		services.AddAuthentication(options =>
		{
			options.DefaultScheme = "cookie";
			options.DefaultChallengeScheme = "oidc";
		}).AddCookie("cookie").AddOpenIdConnect("oidc", options =>
		{
			options.Authority = _serverConfig.OpenIdAuthority;
			options.ClientId = _serverConfig.OpenIdClientId;
			options.ClientSecret = _serverConfig.OpenIdClientSecret;
			options.ResponseType = "code";
			options.RequireHttpsMetadata = false;
			options.ResponseMode = "query";
			options.MapInboundClaims = false; //important, otherwice deletes user sub(guid)
			options.TokenValidationParameters = new()
			{
				RequireExpirationTime = true,
				RequireSignedTokens = true,
				ValidateIssuer = true,
				ClockSkew = TimeSpan.Zero,
				ValidateIssuerSigningKey = false,
				ValidateLifetime = true,
				AuthenticationType = "ApplicationCookie"
			};

			options.Scope.Clear();
			options.Scope.Add("openid");
			options.SaveTokens = true;
		});

		services.AddHsts(_ => { });
		services.AddControllersWithViews();
		services.AddAuthorization();

		services.AddEndpointsApiExplorer();
		services.AddSwaggerGen();
		services.AddRazorPages();
		services.AddSingleton<LobbiesThreadSafe>();
		var configuration = new MapperConfiguration(cfg =>
		{
			cfg.CreateMap<User, UserDto>();
			cfg.CreateMap<Lobby, LobbyDto>();
		});
#if DEBUG
		configuration.AssertConfigurationIsValid();
#endif
		IMapper? mapper = configuration.CreateMapper();
		services.AddSingleton(mapper);
		services.AddSignalR();

		services.AddSingleton(new GameConfig
		{
			HpDecreasePeriod = TimeSpan.FromMilliseconds(500),
			MinHpDecrease = 0,
			MaxHpDecrease = 2,
			StartHp = 10
		});
	}


	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		var webSocketOptions = new WebSocketOptions
		{
			KeepAliveInterval = TimeSpan.FromMinutes(1)
		};
		app.UseWebSockets(webSocketOptions);

		FileExtensionContentTypeProvider contentTypeProvider = new();
		contentTypeProvider.Mappings.Add(".data", "application/octet-stream");
		app.UseCors(AllowEveryOrigins);

		app.UseRouting();
		app.UseAuthentication();
		app.UseAuthorization();
		app.UseHttpsRedirection();
		app.UseStaticFiles();
		// Configure ASP.NET Core endpoints
		app.UseEndpoints(endpoints =>
		{
			endpoints.MapDefaultControllerRoute();
			endpoints.MapRazorPages();
		});

		if (!env.IsDevelopment())
		{
			app.UseExceptionHandler("/Home/Error");
			// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
			app.UseHsts();
		}
		else
		{
			app.UseSwagger();
			app.UseSwaggerUI(options => { options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1"); });
		}
	}
}