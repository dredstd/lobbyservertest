﻿using Core;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class PVPGameLobby : IGameLobby
{
	private readonly CancellationTokenSource HubCancelationSource = new();

	private readonly Lobby _lobby;
	private readonly PVPGame _pvpGame;
	private readonly WebsocketGroup _websocketGroup;
	public Lobby Lobby => _lobby;
	public WebsocketGroup WebsocketGroup => _websocketGroup;

	public PVPGameLobby(Lobby lobby, PVPGame pvpGame, WebsocketGroup websocketGroup)
	{
		_lobby = lobby;
		_pvpGame = pvpGame;
		_websocketGroup = websocketGroup;

		_pvpGame.BattleLog += battleLog => _websocketGroup.SendToAllJson(new GameBattleLog(battleLog));

		_lobby.OnUserJoined += user =>
		{
			_websocketGroup.SendToAllJson(new UserJoinedEvent(user.UserId));
			if (_lobby.IsFull)
			{
				ThreadPool.UnsafeQueueUserWorkItem(RunGame, null);
			}
		};
		_lobby.OnUserLeaved += user =>
		{
			_websocketGroup.RemoveClient(user);
			_websocketGroup.SendToAllJson(new UserLeavedEvent(user.UserId));
			if (_lobby.Users.Count == 0)
			{
				HubCancelationSource.Cancel();
			}
		};
	}

	private async void RunGame(object? state)
	{
		CancellationToken cancellationToken = HubCancelationSource.Token;
		_websocketGroup.SendToAllJson(new GameStartedEvent());
		PVPGameResult pvpGameResult = await _pvpGame.Play(_lobby, cancellationToken);
		await using GameDbContext context = new();

		User? winner = await context.Users.FirstOrDefaultAsync(user => user.UserId == pvpGameResult.Winner.UserId,
			cancellationToken: cancellationToken);
		if (winner != null)
		{
			pvpGameResult.Winner = winner;
		}

		User? looser = await context.Users.FirstOrDefaultAsync(user => user.UserId == pvpGameResult.Looser.UserId,
			cancellationToken: cancellationToken);
		if (looser != null)
		{
			pvpGameResult.Looser = looser;
		}

		_websocketGroup.SendToAllJson(new GameFinishedEvent(pvpGameResult.Winner.UserId, pvpGameResult.Looser.UserId));
		context.GameResults.Add(pvpGameResult);
		await context.SaveChangesAsync(cancellationToken);
	}
}