﻿using Core;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class GameDbContext : DbContext
{
	public DbSet<User> Users { get; set; }
	public DbSet<PVPGameResult> GameResults { get; set; }


	protected override void OnModelCreating(ModelBuilder builder)
	{
		builder.Entity<User>().ToTable("RedRiftTest_Users").HasKey(user => user.UserId);
		builder.Entity<PVPGameResult>().ToTable("RedRiftTest_PVPGameResult");
	}

	protected override void OnConfiguring(DbContextOptionsBuilder options)
	{
		options.UseNpgsql("Host=localhost;Port=5433;Username=postgres;" +
						"Password=mysecretpassword;Database=postgres;Maximum Pool Size=5000",
			builder =>
			{
				builder.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
				builder.MigrationsHistoryTable("RedRiftTest_EFMigrationsHistory");
			});
	}
}