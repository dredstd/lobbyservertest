﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RedRiftTest_Users",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RedRiftTest_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "RedRiftTest_PVPGameResult",
                columns: table => new
                {
                    PVPGameResultId = table.Column<string>(type: "text", nullable: false),
                    WinnerUserId = table.Column<string>(type: "text", nullable: true),
                    LooserUserId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RedRiftTest_PVPGameResult", x => x.PVPGameResultId);
                    table.ForeignKey(
                        name: "FK_RedRiftTest_PVPGameResult_RedRiftTest_Users_LooserUserId",
                        column: x => x.LooserUserId,
                        principalTable: "RedRiftTest_Users",
                        principalColumn: "UserId");
                    table.ForeignKey(
                        name: "FK_RedRiftTest_PVPGameResult_RedRiftTest_Users_WinnerUserId",
                        column: x => x.WinnerUserId,
                        principalTable: "RedRiftTest_Users",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_RedRiftTest_PVPGameResult_LooserUserId",
                table: "RedRiftTest_PVPGameResult",
                column: "LooserUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RedRiftTest_PVPGameResult_WinnerUserId",
                table: "RedRiftTest_PVPGameResult",
                column: "WinnerUserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RedRiftTest_PVPGameResult");

            migrationBuilder.DropTable(
                name: "RedRiftTest_Users");
        }
    }
}
