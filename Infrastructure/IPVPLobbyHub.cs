﻿using Core;

namespace Infrastructure;

public interface IPVPLobbyHub
{
	Task UserJoined(string userId);
	Task UserLeaved(string userId);
	Task GameStarted();
	Task GameFinished(PVPGameResult gameResult);
}

public interface IGameLobby
{

	Lobby Lobby { get; }
	WebsocketGroup WebsocketGroup { get; }
}