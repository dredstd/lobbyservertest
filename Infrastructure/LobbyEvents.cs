﻿namespace Infrastructure;

public class LobbyEvent
{
	public string EventType { get; protected set; }
}

public class UserJoinedEvent : LobbyEvent
{
	public string UserId { get; set; }

	public UserJoinedEvent(string userId)
	{
		EventType = nameof(UserJoinedEvent);
		UserId = userId;
	}
}

public class UserLeavedEvent : LobbyEvent
{
	public string UserId { get; set; }

	public UserLeavedEvent(string userId)
	{
		EventType = nameof(UserLeavedEvent);
		UserId = userId;
	}
}

public class GameStartedEvent : LobbyEvent
{
	public GameStartedEvent()
	{
		EventType = nameof(GameStartedEvent);
	}
}

public class GameBattleLog : LobbyEvent
{
	public string BattleLog { get; set; }

	public GameBattleLog(string battleLog)
	{
		EventType = nameof(GameBattleLog);
		BattleLog = battleLog;
	}
}

public class GameFinishedEvent : LobbyEvent
{
	public string WinnerUserId { get; set; }
	public string LooserUserId { get; set; }

	public GameFinishedEvent(string winnerUserId, string looserUserId)
	{
		EventType = nameof(GameFinishedEvent);
		WinnerUserId = winnerUserId;
		LooserUserId = looserUserId;
	}
}