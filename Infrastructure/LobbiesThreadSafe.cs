﻿using Infrastructure;

namespace Core;

public class LobbiesThreadSafe
{
	private readonly List<IGameLobby> _lobbies = new();

	public void AddLobby(IGameLobby lobby)
	{
		lock (_lobbies)
		{
			if (_lobbies.Contains(lobby))
			{
				throw new Exception("lobby already exists");
			}

			_lobbies.Add(lobby);
		}
	}

	public IGameLobby? GetLobby(string lobbyId)
	{
		lock (_lobbies)
		{
			return _lobbies.FirstOrDefault(lobby => lobby.Lobby.LobbyId == lobbyId);
		}
	}

	public void RemoveLobby(IGameLobby lobby)
	{
		lock (_lobbies)
		{
			_lobbies.Remove(lobby);
		}
	}

	public IGameLobby[] GetLobbies()
	{
		lock (_lobbies)
		{
			return _lobbies.ToArray();
		}
	}
}