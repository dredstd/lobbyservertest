﻿using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using Core;

namespace Infrastructure;

public class WebsocketGroup : IDisposable
{
	private readonly ILogger<WebsocketGroup> _logger;
	private readonly HashSet<WebSocketUser> _aliveClients = new();
	private readonly List<WebSocketUser> _iterateAliveClients = new();

	private readonly List<byte[]> _toDispatch = new();
	private readonly List<byte[]> _iterateToDispatch = new();

	private readonly CancellationTokenSource _cts = new();


	public WebsocketGroup(ILogger<WebsocketGroup> logger)
	{
		_logger = logger;
		Task.Run(PublishEventsWork, _cts.Token);
	}

	public void AddClient(WebSocket webSocket, User user)
	{
		lock (_aliveClients)
		{
			_aliveClients.Add(new WebSocketUser(webSocket, user));
		}
	}

	public void RemoveClient(User user)
	{
		WebSocketUser? clientToDelete;
		lock (_aliveClients)
		{
			clientToDelete = _aliveClients.FirstOrDefault(client => client.User.Equals(user));
		}

		if (clientToDelete != null)
		{
			if (clientToDelete.Socket.State is WebSocketState.Open or WebSocketState.Connecting)
			{
				try
				{
					clientToDelete.Socket.Dispose();
				}
				catch (Exception exception)
				{
					_logger.LogError(exception, "Fail to close websocket");
				}
			}

			RemoveClient(clientToDelete);
		}
	}

	private void RemoveClient(WebSocketUser user)
	{
		lock (_aliveClients)
		{
			_aliveClients.Remove(user);
		}
	}

	public void SendToAllJson<T>(T data)
	{
		lock (_aliveClients)
		{
			if (_aliveClients.Count == 0)
			{
				return;
			}
		}

		var serialized = JsonSerializer.Serialize(data);
		_logger.LogInformation(serialized);
		var bytes = Encoding.UTF8.GetBytes(serialized);
		lock (_toDispatch)
		{
			_toDispatch.Add(bytes);
		}
	}

	public void Dispose()
	{
		_cts.Dispose();
	}


	private async Task PublishEventsWork()
	{
		while (!_cts.IsCancellationRequested)
		{
			_iterateAliveClients.Clear();
			lock (_aliveClients)
			{
				_iterateAliveClients.AddRange(_aliveClients);
			}

			_iterateToDispatch.Clear();
			lock (_toDispatch)
			{
				_iterateToDispatch.AddRange(_toDispatch);
				_toDispatch.Clear();
			}

			if (_iterateAliveClients.Count != 0)
			{
				for (int i = 0; i < _iterateAliveClients.Count; i++)
				{
					WebSocketUser client = _iterateAliveClients[i];
					if (client.Socket.State
						is WebSocketState.Aborted
						or WebSocketState.Closed
						or WebSocketState.CloseReceived
						or WebSocketState.CloseSent)
					{
						_iterateAliveClients.RemoveAt(i);
						lock (_aliveClients)
						{
							_aliveClients.Remove(client);
						}
					}
				}

				foreach (byte[] message in _iterateToDispatch)
				{
					foreach (WebSocketUser client in _iterateAliveClients)
					{
						ThreadPool.UnsafeQueueUserWorkItem(PublishMessageWorker, new WorkContext(client, message));
					}
				}
			}

			await Task.Delay(10);
		}
	}

	private async void PublishMessageWorker(object? state)
	{
		if (state is WorkContext workContext)
		{
			try
			{
				if (workContext.Client.Socket.State is WebSocketState.Aborted or WebSocketState.Closed)
				{
					RemoveClient(workContext.Client);
				}
				else
				{
					await workContext.Client.Socket.SendAsync(workContext.Message, WebSocketMessageType.Text,
						WebSocketMessageFlags.EndOfMessage, _cts.Token);
				}
			}
			catch (Exception exception)
			{
				_logger.LogError(exception: exception, "Fail to publish client websocket");

				try
				{
					workContext.Client.Socket.Dispose();
				}
				catch (Exception exceptionDispose)
				{
					_logger.LogError(exception: exceptionDispose, "Fail to dispose client websocket");
				}

				RemoveClient(workContext.Client);
			}
		}
	}

	private class WorkContext
	{
		public readonly WebSocketUser Client;
		public readonly byte[] Message;

		public WorkContext(WebSocketUser client, byte[] message)
		{
			Client = client;
			Message = message;
		}
	}

	private class WebSocketUser
	{
		public readonly WebSocket Socket;
		public readonly User User;

		public WebSocketUser(WebSocket socket, User user)
		{
			Socket = socket;
			User = user;
		}
	}
}