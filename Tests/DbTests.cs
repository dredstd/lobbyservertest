﻿using Core;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Tests;

public class DbTests
{
	private readonly User _user1 = new("_user1");
	private readonly User _user2 = new("_user2");

	[Test]
	public async Task AddPVPResultToDb()
	{
		await using (var context = new GameDbContext())
		{
			User? dbUser1 = await context.Users.FirstOrDefaultAsync(user => user.UserId == _user1.UserId);
			dbUser1 ??= _user1;
			User? dbUser2 = await context.Users.FirstOrDefaultAsync(user => user.UserId == _user2.UserId);
			dbUser2 ??= _user2;
			var pvpGameResult = new PVPGameResult(dbUser1, dbUser2);
			context.GameResults.Add(pvpGameResult);
			await context.SaveChangesAsync();
		}

		await using (var context = new GameDbContext())
		{
			PVPGameResult? pvpResult = await context.GameResults.FirstOrDefaultAsync(result => result.Winner.UserId.Equals(_user1.UserId));
			Assert.That(pvpResult != null);
			context.GameResults.Remove(pvpResult!);
			context.Users.Remove(_user1);
			context.Users.Remove(_user2);
			await context.SaveChangesAsync();
		}
	}
}