using Core;

namespace Tests;

public class LobbyTests
{
	private readonly User _user1 = new("_user1");
	private readonly User _user2 = new("_user2");

	[Test]
	public void HostConstructed()
	{
		var totalPlayers = 2;
		var lobby = new Lobby(_user1, totalPlayers);
		Assert.That(lobby.Host == _user1);
		Assert.That(lobby.Capacity == totalPlayers);
		Assert.That(lobby.IsFull == false);
		Assert.That(lobby.Users.Count == 1);
		Assert.That(lobby.Users.Contains(_user1));
	}

	[Test]
	public void JoinLobbyTwice()
	{
		var totalPlayers = 2;
		var lobby = new Lobby(_user1, totalPlayers);
		Assert.Throws<InvalidOperationException>(() => lobby.Join(_user1));
	}

	[Test]
	public void JoinLobbyFull()
	{
		var totalPlayers = 1;
		var lobby = new Lobby(_user1, totalPlayers);
		Assert.Throws<InvalidOperationException>(() => lobby.Join(_user2));
	}

	[Test]
	public void JoinLobbyk()
	{
		var totalPlayers = 2;
		var lobby = new Lobby(_user1, totalPlayers);
		lobby.Join(_user2);
		Assert.That(lobby.Users.Contains(_user2));
	}

	[Test]
	public void LeaveLobbyOk()
	{
		var totalPlayers = 2;
		var lobby = new Lobby(_user1, totalPlayers);
		lobby.Join(_user2);
		lobby.Leave(_user2);
		Assert.That(!lobby.Users.Contains(_user2));
	}
}