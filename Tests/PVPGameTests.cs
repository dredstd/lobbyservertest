﻿using Core;

namespace Tests;

public class PVPGameTests
{
	public readonly User _user1 = new("_user1");
	public readonly User _user2 = new("_user2");
	public readonly User _user3 = new("_user3");

	private readonly GameConfig _gameConfig = new()
	{
		HpDecreasePeriod = TimeSpan.Zero,
		MinHpDecrease = 0,
		MaxHpDecrease = 2,
		StartHp = 10
	};

	[Test]
	public Task FailStartNotReadyLobby()
	{
		var pvpGame = new PVPGame(_gameConfig);
		var lobby = new Lobby(_user1, 2);
		Assert.ThrowsAsync<InvalidOperationException>(async () => await pvpGame.Play(lobby, CancellationToken.None));
		return Task.CompletedTask;
	}

	[Test]
	public Task FailStartNotLobbySizeNot2()
	{
		var pvpGame = new PVPGame(_gameConfig);
		var lobby = new Lobby(_user1, 3);
		lobby.Join(_user2);
		lobby.Join(_user3);
		Assert.ThrowsAsync<InvalidOperationException>(async () => await pvpGame.Play(lobby, CancellationToken.None));
		return Task.CompletedTask;
	}

	[Test]
	public async Task CheckWinner()
	{
		var pvpGame = new PVPGame(_gameConfig);
		var lobby = new Lobby(_user1, 2);
		lobby.Join(_user2);
		PVPGameResult pvpGameResult = await pvpGame.Play(lobby, CancellationToken.None);
		Assert.That(pvpGameResult.Winner.Equals(_user1) || pvpGameResult.Winner.Equals(_user2));
		Assert.That(pvpGameResult.Looser.Equals(_user1) || pvpGameResult.Looser.Equals(_user2));
		Assert.That(!pvpGameResult.Looser.Equals(pvpGameResult.Winner));
	}
}