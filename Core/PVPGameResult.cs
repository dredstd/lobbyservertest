﻿using System.ComponentModel.DataAnnotations;

namespace Core;

public class PVPGameResult
{
	[Key]
	public string PVPGameResultId { get; set; }

	public User Winner { get; set; }

	public User Looser { get; set; }

	private PVPGameResult()
	{
	}

	public PVPGameResult(User winner, User looser)
	{
		PVPGameResultId = Guid.NewGuid().ToString();
		Winner = winner;
		Looser = looser;
	}
}