﻿namespace Core;

public class GameConfig
{
	public int MinHpDecrease { get; init; }
	public int MaxHpDecrease { get; init; }
	public int StartHp { get; init; }
	public TimeSpan HpDecreasePeriod { get; init; }
}