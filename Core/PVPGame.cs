﻿using System.Text;

namespace Core;

public class PVPGame
{
	private readonly GameConfig _gameConfig;
	public event Action<string>? BattleLog;
	public PVPGame(GameConfig gameConfig)
	{
		_gameConfig = gameConfig;
	}

	public async Task<PVPGameResult> Play(Lobby lobby, CancellationToken cancellationToken)
	{
		if (!lobby.IsFull)
		{
			throw new InvalidOperationException("lobby is not full");
		}

		if (lobby.Users.Count != 2)
		{
			throw new InvalidOperationException("players count is not 2. PvP game requires exactly 2");
		}

		Player[] players = lobby.Users.Select(user => new Player(user, _gameConfig.StartHp)).ToArray();
		StringBuilder stringBuilder = new StringBuilder();
		PVPGameResult? gameResult = null;
		int step = 0;
		while (gameResult == null)
		{
			int deadPlayers = 0;
			stringBuilder.Clear();
			stringBuilder.Append($"Step {step++}: ");
			foreach (Player player in players)
			{
				var playerDamage = Random.Shared.Next(_gameConfig.MinHpDecrease, _gameConfig.MaxHpDecrease + 1);
				player.Health -= playerDamage;
				stringBuilder.Append($"{player.User.UserId} - {playerDamage} = {player.Health}; ");
				if (player.Health <= 0)
				{
					deadPlayers++;
				}
			}

			BattleLog?.Invoke(stringBuilder.ToString());

			switch (deadPlayers)
			{
				case 0:
					await Task.Delay(_gameConfig.HpDecreasePeriod, cancellationToken);
					break;
				case 1:
				{
					User looser = players.First(player => player.Health <= 0).User;
					User winner = players.First(player => !player.User.Equals(looser)).User;
					gameResult = new PVPGameResult(winner, looser);
					break;
				}
				case 2:
				{
					//Randomize winner to avoid host priviledge
					int looserIndex = Random.Shared.Next(0, 2);
					User looser = players.Where(player => player.Health <= 0).Skip(looserIndex).First().User;
					User winner = players.First(player => !player.User.Equals(looser)).User;
					gameResult = new PVPGameResult(winner, looser);
					break;
				}
			}
		}

		return gameResult;
	}
}