﻿using System.ComponentModel.DataAnnotations;

namespace Core;

public class User
{
	[Key]
	public string UserId { get; set; }


	private User()
	{
	}

	public User(string userId)
	{
		UserId = userId;
	}

	protected bool Equals(User other)
	{
		return UserId == other.UserId;
	}

	public override bool Equals(object? other)
	{
		if (ReferenceEquals(null, other)) return false;
		if (ReferenceEquals(this, other)) return true;
		if (other.GetType() != GetType()) return false;
		return Equals((User)other);
	}

	public override int GetHashCode()
	{
		return UserId.GetHashCode();
	}
}