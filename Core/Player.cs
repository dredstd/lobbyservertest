﻿namespace Core;

public class Player
{
	public User User { get; }
	public int Health { get; set; }

	public Player(User user, int health)
	{
		User = user;
		Health = health;
	}

	public override string ToString()
	{
		return $"Player {User.UserId}";
	}
}