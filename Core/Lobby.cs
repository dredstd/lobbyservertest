﻿namespace Core;

public class Lobby
{
	public string LobbyId { get; }
	public int Capacity { get; }
	private readonly List<User> _users;
	public IReadOnlyCollection<User> Users => _users;

	public User Host => _users[0];

	public bool IsFull => _users.Count == Capacity;

	public event Action<User>? OnUserJoined;
	public event Action<User>? OnUserLeaved;

	public Lobby(User host, int totalUsers)
	{
		if (totalUsers < 1)
		{
			throw new ArgumentException("Can't create lobby without users");
		}

		LobbyId = Guid.NewGuid().ToString();
		Capacity = totalUsers;
		_users = new List<User>(Capacity)
		{
			host
		};
	}

	public void Join(User user)
	{
		if (_users.Count == Capacity)
		{
			throw new InvalidOperationException("Lobby is full");
		}

		if (_users.Contains(user))
		{
			throw new InvalidOperationException($"Already joined {user}");
		}

		_users.Add(user);
		OnUserJoined?.Invoke(user);
	}


	public void Leave(User user)
	{
		if (!_users.Contains(user))
		{
			throw new InvalidOperationException($"There is no user {user}");
		}

		_users.Remove(user);
		OnUserLeaved?.Invoke(user);
	}
}