## Lobbies server and game simulation
Multithreading server. Stores lobbies in memory. Persists game results to postgres

### Endpoints
![swagger](/swagger.png)

### Events

User is allowed to start listen lobby events after joining to a lobby
![swagger](/websocket_result.png)

### Tests
Some tests for lobbies, game and integration test for db

![img.png](nunit.png)

### Used tech
<ul>
<li>asp.net core 7</li>
<li>EF core 7</li>
<li>AutoMapper</li>
<li>Websockets</li>
<li>Swashbucle</li>
<li>NUnit</li>
</ul> 


### Task description

3 players (clients) are in the game. One player creates a room (lobby) and is treated as a host. Other in-game players attempt to join the room, but only 2 players (including the host player) can be in the room at the same time. Whenever the room is full (2 players joined the room, total 2 players), the game will start automatically.

Each player has a “Health=default(10)” parameter. During the game players' health randomly decreases by 0-2 each second until one of the players dies (health<=0). Room must have only one winner: case when one of the players is alive and another player dies, afterwards the room closes, reporting status to the players and saving it into the database. No draw is possible.
